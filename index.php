<?php 
    $fileContent = file_get_contents("nodered-config.json");
    $config = json_decode($fileContent);

    $maximoFileContent = file_get_contents("dataset.json");
    $maximo = json_decode($maximoFileContent);
    $maximo = $maximo->QuerySNDASSET2Response->ASSET;

    $sensorOptions = 
    [
        "Nothing",
        "Current Transducer",
        "VVB010 Vibration Sensor",
        "TV7105 Temperature Sensor"
    ]
?>

<html>
    <head>
        <link rel="stylesheet" href="style.css"/>
    </head>

    <body>
        <center>
            <img src="logo.png"/>
            <div class="centerContainer">
            
                <form action="saveconfig.php">
                    <?php
                        if(isset($_GET["applied"]))
                        {
                            echo '<script>alert("Settings applied!");</script>';
                        }

                        $vars = get_object_vars ( $config );

                        foreach($vars as $key=>$value) 
                        {
                            echo '<div class="configLine">';

                            $ccWord = $key;
                            $re = '/(?#! splitCamelCase Rev:20140412)
                                # Split camelCase "words". Two global alternatives. Either g1of2:
                                (?<=[a-z])      # Position is after a lowercase,
                                (?=[A-Z])       # and before an uppercase letter.
                                | (?<=[A-Z])      # Or g2of2; Position is after uppercase,
                                (?=[A-Z][a-z])  # and before upper-then-lower case.
                                /x';
                            $a = preg_split($re, $ccWord);

                            $transformed = ucfirst(implode(" ", $a));

                            if(is_object($value))
                            {
                                if(isset($value->sensorType))
                                {
                                    echo '<label class="label" style="float:left" for="' . $key . '">' . $transformed . '</label>:';
                                    echo '<select name="' . $key . '" id="' . $key . '">';
                                    for($i = 0; $i < count($sensorOptions); $i++)
                                    {
                                        echo '<option value="' . $i . '"' . ($value->sensorType == $i? 'selected="selected"' : '') . '>' . $sensorOptions[$i] . '</option>';
                                    }
                                    echo '</select>';
                                }
                            }
                            else if(is_bool($value))
                            {
                                echo '<label class="label" style="float:left" for="' . $key . '">' . $transformed . '</label>:';
                                echo '<input type="checkbox" id="' . $key . '" name="' . $key . '"' . ($value? ' checked="true"' : '') . ' style="float:right">';
                            }
                            else
                            {
                                echo  '<span class="label" style="float:left">' . $transformed . '</span>: <input id="' . $key . '" name="' . $key . '" type="' . (is_string($value)? "text" : "number") . '" value="' . $value . '" style="float: right"/>';
                            }

                            echo '</div>';
                        }

                        //MAXIMO

                        $vars = get_object_vars ( $maximo );

                        foreach($vars as $key=>$value) 
                        {
                            echo '<div class="configLine">';

                            $ccWord = $key;
                            $re = '/(?#! splitCamelCase Rev:20140412)
                                # Split camelCase "words". Two global alternatives. Either g1of2:
                                (?<=[a-z])      # Position is after a lowercase,
                                (?=[A-Z])       # and before an uppercase letter.
                                | (?<=[A-Z])      # Or g2of2; Position is after uppercase,
                                (?=[A-Z][a-z])  # and before upper-then-lower case.
                                /x';
                            $a = preg_split($re, $ccWord);

                            $transformed = ucfirst(implode(" ", $a));

                            
                            echo  '<span class="label" style="float:left">' . $transformed . '</span>: <span class="label" id="' . $key . '" name="' . $key . '"  style="float: right">' . $value . '</span>';
                            

                            echo '</div>';
                        }
                    ?>
                    <input type="submit" value="Apply"/>
                </form>
            </div>
            <img src="cwd.png"/>
        </center>
    </body>
</html>
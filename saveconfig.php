<?php
$fileContent = file_get_contents("nodered-config.json");
$config = json_decode($fileContent);

$vars = get_object_vars ( $config );

foreach($vars as $key=>$value) 
{
    if(is_object($value))
    {
        if(isset($value->sensorType))
        {
            $config->{$key}->sensorType = $_GET[$key];
        }
    }
    else if(is_bool($value))
    {
        if(!isset($_GET[$key]))
            $_GET[$key] = false;

        $config->{$key} = $_GET[$key] == "true" ||  $_GET[$key] == "on";
    }
    else
    {
        if(is_string($value))
        {
            $config->{$key} = $_GET[$key];
        }
        else
        {
            $config->{$key} = (double)$_GET[$key];
        }
    }
}

file_put_contents("nodered-config.json", json_encode($config));

//print_r($config);
header('Location: ' . $_SERVER['HTTP_REFERER'] . '?applied=true');